# PyDoist

# PyDoist

A python script to display the most important todos from Todoist.

The Todoist API key is read from your default keyring.
A script for inserting an API key into your keyring is included.