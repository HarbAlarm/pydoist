
install_requires = ["todoist-python", "keyring"]

import requests, keyring

key = str(keyring.get_password("todoist", "default"))
requestData = None

if not key or key == "None":
    print("LOCKED KEYRING / MISSING KEY")
    exit(1)


def request(url, param):
    global requestData
    requestData = requests.get(
        "https://beta.todoist.com/API/v8/" + url,
        params={"filter": param},
        headers={"Authorization": "Bearer %s" % key}
    ).json()
    return requestData


if not (request("tasks", "p1")):
    if not (request("tasks", "p2")):
        if not (request("tasks", "p3")):
            if not (request("tasks", "today")):
                request("tasks", "overdue")

print(requestData[0]["content"])

